package laali.ir.ftb;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

public class FTBData implements FTBListener{
	private Map<String, String> sentParse = new TreeMap<>();
	
	public FTBData(String fld) throws ParserConfigurationException, SAXException, IOException {
		@SuppressWarnings("unchecked")
		Collection<File> listFiles = FileUtils.listFiles(new File(fld), new String[]{".xml"}, true);
		
		FTBReader reader = new FTBReader();
		for (File file: listFiles){
			reader.parse(file, this);
		}
		
	}
	
	@Override
	public void sent(String sentId, String parse) {
		sentParse.put(sentId, parse);
	}

	public String get(String sentId){
		return sentParse.get(sentId);
	}
}
