package laali.ir.ftb;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

enum FTBElement{
	SENT, NON, w
}

public class FTBReader extends DefaultHandler{
	private static SAXParserFactory parserFactory = SAXParserFactory.newInstance();
	
	private FTBListener listener;
	
	private String sentId;
	private File parsedFile;
	private StringBuilder parse = new StringBuilder();
	private StringBuilder buffer = new StringBuilder();
	private LinkedList<Boolean> considerElements = new LinkedList<Boolean>();

	public void parse(String aParse, FTBListener ftbListener) throws ParserConfigurationException, SAXException, IOException {
		this.listener = ftbListener;
		InputStream stream = new ByteArrayInputStream(aParse.getBytes(StandardCharsets.UTF_8));
		
		SAXParser parser = parserFactory.newSAXParser();
		parser.parse(stream, this);
	}
	
	public void parse(File file, FTBListener ftbListener) throws ParserConfigurationException, SAXException, IOException {
		SAXParser parser = parserFactory.newSAXParser();
		this.listener = ftbListener;
		
		this.parsedFile = file;
		parser.parse(file, this);
	}
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		FTBElement element = getElement(qName);

		switch (element) {
		case SENT:
			sentId = attributes.getValue("nb");
			parse.setLength(0);
			openConstituent("S");
			break;
		case w:
			buffer.setLength(0);
			String cat = attributes.getValue("cat");
			String escaped = shouldBeEscaped(attributes.getValue("lemma"));
			if (cat == null)
				cat = attributes.getValue("catint");
			if (escaped != null)
				openConstituent(escaped);
			else if (!isCompound(attributes))
				openConstituent(cat);
			break;
		default:
			openConstituent(qName);
			break;
		}
		
		if (isCompound(attributes))
			considerElements.push(false);
		else
			considerElements.push(true);
	}

	private boolean isCompound(Attributes attributes) {
		return "yes".equals(attributes.getValue("compound"));
	}
	
	private String shouldBeEscaped(String word){
		String normalWord = null;
		if (word == null)
			return word;
		switch (word) {
		case "(":
			normalWord = "-LRB-";
			break;
		case ")":
			normalWord = "-RRB-";
			break;
		default:
			break;
		}
		return normalWord;
	}

	private void openConstituent(String qName) {
		parse.append("(");
		parse.append(qName);
		parse.append(" ");
	}

	private FTBElement getElement(String qName) {
		FTBElement element = FTBElement.NON;
		try {
			element = FTBElement.valueOf(qName);
		} catch (IllegalArgumentException e) {
		}
		return element;
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		buffer.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (!considerElements.pop())
			return;
		
		FTBElement element = getElement(qName);
		switch (element) {
		case SENT:
			String name = "";
			if (parsedFile != null){
				name = parsedFile.getName();
				name = name.substring(0, name.length() - "xml".length());
			}
			closeConstituent("");
			listener.sent(name + sentId, parse.toString().trim());
			break;
		case w:
			String word = buffer.toString().trim();
			if (shouldBeEscaped(word) != null)
				word = shouldBeEscaped(word);
			closeConstituent(word);
			break;
		default:
			closeConstituent("");
			break;
		}
	}

	private void closeConstituent(String terminal) {
		parse.append(terminal + ") ");
	}
	
}
