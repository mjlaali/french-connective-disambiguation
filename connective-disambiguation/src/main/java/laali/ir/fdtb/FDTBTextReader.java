package laali.ir.fdtb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.token.type.Sentence;
import org.cleartk.util.ViewUriUtil;
import org.xml.sax.SAXException;

public class FDTBTextReader  extends JCasAnnotator_ImplBase implements FDTBListener{
	private StringBuilder content = new StringBuilder();
	private List<Integer> sentStarts = new ArrayList<Integer>();
	private List<Integer> sentEnds = new ArrayList<Integer>();

	public static AnalysisEngineDescription getDescription() throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(
				FDTBTextReader.class);
	}

	@Override
	public void addSent(String sent, String sentId) {
		sentStarts.add(content.length());
		
		content.append(sent);
		content.append("\n");
		
		sentEnds.add(content.length());
	}

	@Override
	public void addConnective(List<Integer> connStarts, List<Integer> connEnds) {
	}

	@Override
	public void startArticle(String id) {
	}

	@Override
	public void endArticle() {
	}

	@Override
	public void finished() {
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		File file = new File(ViewUriUtil.getURI(aJCas));
		
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		SAXParser parser;
		try {
			content = new StringBuilder();
			sentStarts = new ArrayList<Integer>();
			sentEnds = new ArrayList<Integer>();
			
			parser = parserFactory.newSAXParser();
			parser.parse(file, new FDTBReader(this));
			
			aJCas.setSofaDataString(content.toString(), "text/plain");
			
			for (int i = 0; i < sentStarts.size(); i++){
				new Sentence(aJCas, sentStarts.get(i), sentEnds.get(i)).addToIndexes();
			}
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		

	}

}
