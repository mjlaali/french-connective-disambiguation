package laali.ir.fdtb;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.util.ViewUriUtil;
import org.xml.sax.SAXException;

public class GoldConnectiveAnnotator extends JCasAnnotator_ImplBase implements FDTBListener{
	
	public static AnalysisEngineDescription getDescription() throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(
				GoldConnectiveAnnotator.class);
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		File file = new File(ViewUriUtil.getURI(jCas));
	
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		SAXParser parser;
		try {
			
			parser = parserFactory.newSAXParser();
			parser.parse(file, new FDTBReader(this));
			
			
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		
	}

	public void addSent(String sent, String sentId) {
	}

	public void addConnective(List<Integer> connStarts, List<Integer> connEnds) {
	}

	public void startArticle(String id) {
		
	}

	public void endArticle() {
		
	}

	public void finished() {
		
	}

}
