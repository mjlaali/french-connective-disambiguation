package laali.ir.fdtb;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.conll2015.SyntaxReader;
import org.cleartk.token.type.Sentence;
import org.cleartk.token.type.Token;
import org.cleartk.util.treebank.TreebankFormatParser;

public class FDTBSyntaxReader  extends JCasAnnotator_ImplBase {
	public static final String PARAM_SYNTAX_JSON_FILE = "PARAM_SYNTAX_JSON_FILE";

	@ConfigurationParameter(
			name = PARAM_SYNTAX_JSON_FILE,
			description = "The syntax file contains the phrase structures of the sentences.",
			mandatory = true)
	private String syntaxFilePath;

	private SyntaxReader syntaxReader = new SyntaxReader();
	private List<String> sentSyntaxTrees;
	public static AnalysisEngineDescription getDescription(String syntaxFile) throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(
				FDTBSyntaxReader.class, 
				PARAM_SYNTAX_JSON_FILE, 
				syntaxFile);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		try {
			sentSyntaxTrees = FileUtils.readLines(new File(syntaxFilePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		Collection<Sentence> sents = JCasUtil.select(aJCas, Sentence.class);
		int idx = 0;
		for (Sentence sent: sents){
			String parseTree = sentSyntaxTrees.get(idx);
			List<Token> sentTokens = addToken(sent, parseTree, aJCas);
			syntaxReader.addSyntacticConstituents(aJCas, sentTokens, parseTree);
			idx++;
		}
	}

	private List<Token> addToken(Sentence sent, String parseTree, JCas aJCas) {
		
		org.cleartk.util.treebank.TopTreebankNode topNodes = TreebankFormatParser.parse(parseTree);
		List<org.cleartk.util.treebank.TreebankNode> leaves = new ArrayList<org.cleartk.util.treebank.TreebankNode>(); 
		addLeaves(topNodes, leaves);

		List<Token> tokens = new ArrayList<Token>();
		int begin = sent.getBegin();
		String sentText = sent.getCoveredText();
		int offset = 0;
		for (org.cleartk.util.treebank.TreebankNode leaf: leaves){
			while (Character.isWhitespace(sentText.charAt(offset))){
				offset++;
			}
			
			String tokenString = leaf.getText();
			if (!sentText.startsWith(leaf.getText(), offset)){
//				while 
			}
		}
		return tokens;
	}

	private void addLeaves(org.cleartk.util.treebank.TreebankNode topNodes, List<org.cleartk.util.treebank.TreebankNode> leaves) {
		if (topNodes.isLeaf()){
			leaves.add(topNodes);
		} else {
			for (org.cleartk.util.treebank.TreebankNode aNode: topNodes.getChildren()){
				addLeaves(aNode, leaves);
			}
		}
			
	}

}
