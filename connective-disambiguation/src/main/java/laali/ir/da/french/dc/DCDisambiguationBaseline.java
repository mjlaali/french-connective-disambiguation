package laali.ir.da.french.dc;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import laali.ir.fdtb.FDTB;
import laali.ir.fdtb.FDTBTextReader;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.cleartk.util.cr.UriCollectionReader;
import org.cleartk.util.cr.linereader.LineReaderXmiWriter;

public class DCDisambiguationBaseline {
	
	private void readFDTB() throws UIMAException, IOException{
		@SuppressWarnings("unchecked")
		Collection<File> files = FileUtils.listFiles(new File(FDTB.FDTB_DIR), new String[]{"xml"}, false);

		CollectionReaderDescription reader = UriCollectionReader.getDescriptionFromFiles(files);

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(FDTBTextReader.getDescription());
		builder.add(LineReaderXmiWriter.getDescription(new File(FDTB.XMI_DIR)));

		//read the FDTB
		SimplePipeline.runPipeline(reader, builder.createAggregateDescription());
	}
	
	private void extractDCList(){
		//extract the list of discourse connectives in the FDTB.
	}
	
	private void reportStatistics(){
		//report the number of occurrence of each connectives and discourse usage of it.
	}
	
	public void run() throws UIMAException, IOException{
		readFDTB();
		extractDCList();
		reportStatistics();
	}

	public static void main(String[] args) throws UIMAException, IOException {
		DCDisambiguationBaseline dcDisambiguationBaseline = new DCDisambiguationBaseline();
		dcDisambiguationBaseline.run();
		
	}
}
