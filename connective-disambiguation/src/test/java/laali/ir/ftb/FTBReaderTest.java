package laali.ir.ftb;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.xml.sax.SAXException;

public class FTBReaderTest {
	@Mock FTBListener ftbListener;
	private FTBReader ftbReader = new FTBReader();

	@Before
	public void setUp() throws ParserConfigurationException, SAXException, IOException{
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void whenReadingAFTBFileThenForEachSentAParanthesisTextIsGenerated() throws ParserConfigurationException, SAXException, IOException {
		File file = new File("data/FrenchTreebank/corpus-constit/lmf3_01000_01499ep.aa.xml");
		ftbReader.parse(file, ftbListener);
		verify(ftbListener, times(494)).sent(anyString(), anyString());
	}

	@Test
	public void whenReadingSentsFromAFTBFileThenSentIDOfTheFirstSentIsOK() throws ParserConfigurationException, SAXException, IOException{
		File file = new File("data/FrenchTreebank/corpus-constit/lmf3_01000_01499ep.aa.xml");
		ftbReader.parse(file, ftbListener);
		verify(ftbListener).sent(eq("lmf3_01000_01499ep.aa.1000"), anyString());
	}

	@Test
	public void whenReadingParseFromAnXMLThenConstituentsAreInsertedToParenthesisesString() throws ParserConfigurationException, SAXException, IOException{
		String xml = "<SENT nb=\"1000\"> <NP> <PP> <NP> </NP> </PP></NP> </SENT>";
		String parse = "(S (NP (PP (NP ) ) ) )";
		ftbReader.parse(xml, ftbListener);
		verify(ftbListener).sent(eq("1000"), eq(parse));
	}

	@Test
	public void whenReadingParseFromAnXMLThenWordsAreInsertedToParenthesisesString() throws ParserConfigurationException, SAXException, IOException{
		String xml = "<SENT nb=\"1000\"> <VPpart>\n"
				+ "<w cat=\"PONCT\" ee=\"PONCT-W\" ei=\"PONCTW\" lemma=\",\" subcat=\"W\">,</w>\n" 
				+ "<w cat=\"ADV\" ee=\"ADV\" ei=\"ADV\" lemma=\"seulement\">seulement</w>\n" 
				+ "<w cat=\"V\" ee=\"V--Kmp\" ei=\"VKmp\" lemma=\"blesser\" mph=\"Kmp\" subcat=\"\">blessés</w>\n" 
				+ "<w cat=\"PONCT\" ee=\"PONCT-W\" ei=\"PONCTW\" lemma=\",\" subcat=\"W\">,</w>\n" 
				+ "</VPpart> </SENT>";
		String parse = "(S (VPpart (PONCT ,) (ADV seulement) (V blessés) (PONCT ,) ) )";
		ftbReader.parse(xml, ftbListener);
		verify(ftbListener).sent(eq("1000"), eq(parse));

	}

	@Test
	public void whenReadingCompoundNounThenAllWordsApearWithCatintTag() throws ParserConfigurationException, SAXException, IOException{
		String xml = "<SENT nb=\"1000\"> \n" 
				+  "<PP> \n "
				+ "<w cat=\"P\" ee=\"P\" ei=\"P\" lemma=\"en\">en</w> \n"
				+ "<NP> \n "
				+ "<w compound=\"yes\" cat=\"N\" ee=\"N-P-fs\" ei=\"NPfs\" lemma=\"C\u00f4te-d\'Ivoire\" mph=\"fs\" subcat=\"P\"> \n "
				+ "<w catint=\"N\">Côte</w>\n "
				+ "<w catint=\"PONCT\">-</w>\n "
				+ "<w catint=\"P\">d'</w>\n "
				+ "<w catint=\"N\">Ivoire</w>\n "
				+ "</w> \n </NP> \n </PP>\n </SENT>";
		String parse = "(S (PP (P en) (NP (N Côte) (PONCT -) (P d') (N Ivoire) ) ) )";
		ftbReader.parse(xml, ftbListener);
		verify(ftbListener).sent(eq("1000"), eq(parse));
	}

	@Test
	public void whenTokenContainsParenthesisThenParentesisesAreConvertedToLRB_RRB() throws ParserConfigurationException, SAXException, IOException{
		String xml = "<SENT nb=\"392\">\n"
				+ "<NP> \n"
				+ "<w cat=\"N\" compound=\"yes\" ee=\"N-P-s\" ei=\"NPs\" lemma=\"SAINT-DENIS\" mph=\"s\" subcat=\"P\">\n"
				+ "<w catint=\"A\">SAINT</w>\n"
				+ "<w catint=\"PONCT\">-</w>\n"
				+ "<w catint=\"N\">DENIS</w>\n"
				+ "</w> \n"
				+ "<w cat=\"PONCT\" ee=\"PONCT-W\" ei=\"PONCTW\" lemma=\"(\" subcat=\"W\">(</w> \n"
				+ "<NP> \n"
				+ "<w cat=\"N\" ee=\"N-P-fs\" ei=\"NPfs\" lemma=\"Réunion\" mph=\"fs\" subcat=\"P\">Réunion</w> \n"
				+ "</NP> \n"
				+ "<w cat=\"PONCT\" ee=\"PONCT-W\" ei=\"PONCTW\" lemma=\")\" subcat=\"W\">)</w>\n"
				+ "</NP>\n"
				+ "</SENT>";
		String parse = "(S (NP (A SAINT) (PONCT -) (N DENIS) (-LRB- -LRB-) (NP (N Réunion) ) (-RRB- -RRB-) ) )";
		ftbReader.parse(xml, ftbListener);
		verify(ftbListener).sent(eq("392"), eq(parse));

	}
}
