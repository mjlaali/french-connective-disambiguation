package laali.ir.fdtb;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.syntax.constituent.type.TopTreebankNode;
import org.cleartk.syntax.constituent.type.TreebankNode;
import org.cleartk.token.type.Token;
import org.cleartk.util.cr.UriCollectionReader;
import org.junit.Before;
import org.junit.Test;

public class FDTBSyntaxReaderTest {
	private static final String SYNTAX_FILE = "data/sents.parsed.txt";
	private JCas jCas;

	@Before
	public void setUp() throws UIMAException, IOException{
		@SuppressWarnings("unchecked")
		Collection<File> files = FileUtils.listFiles(new File(FDTB.FDTB_DIR), new String[]{"xml"}, false);

		assertThat(files).hasSize(1);
		CollectionReaderDescription reader = UriCollectionReader.getDescriptionFromFiles(files);

		AnalysisEngineDescription fdtbTextReader = FDTBTextReader.getDescription();
		AnalysisEngineDescription fdtbSyntaxReader = FDTBSyntaxReader.getDescription(SYNTAX_FILE);
		
		for (JCas jCas : SimplePipeline.iteratePipeline(reader, fdtbTextReader, fdtbSyntaxReader)) {
			assertThat(this.jCas).isNull();
			this.jCas = jCas;
		}
	}
	
	@Test
	public void givenTheParsedSentWhenReadingTheParsedTextsThenTreebankAnnotationsAreAddedToDoucment(){
		Collection<TreebankNode> select = JCasUtil.select(jCas, TreebankNode.class);
		assertThat(select).isNotNull();
		assertThat(select.size()).isGreaterThan(0);
	}
	
	@Test
	public void givenSecondSentWhenReadingTheParsedInformationThenAllConstituentsHaveCorrectText(){
		Collection<TopTreebankNode> topNodes = JCasUtil.select(jCas, TopTreebankNode.class);
		
		Iterator<TopTreebankNode> iterNodes = topNodes.iterator();
		iterNodes.next();
		TopTreebankNode secondTopTreeNode = iterNodes.next();
		
		String secondSentText = "En 1989 , Chargeurs SA a considérablement renforcé sa branche textile en rachetant Prouvost .";
		assertThat(secondTopTreeNode.getCoveredText()).isEqualTo(secondSentText);
		
		List<Token> tokens = JCasUtil.selectCovered(Token.class, secondTopTreeNode);
		String[] words = secondSentText.split(" ");
		for (int i = 0; i < words.length; i++){
			assertThat(tokens.get(i).getCoveredText()).isEqualTo(words[i]);
		}
		
	}
	
}
