package laali.ir.fdtb;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.token.type.Sentence;
import org.cleartk.util.cr.UriCollectionReader;
import org.junit.Before;
import org.junit.Test;

public class FDTBTextReaderTest {
	private JCas jCas;

	@Before
	public void setUp() throws UIMAException, IOException{
		@SuppressWarnings("unchecked")
		Collection<File> files = FileUtils.listFiles(new File("data/fdtb/fdtb1"), new String[]{"xml"}, false);

		assertThat(files).hasSize(1);
		// A collection reader that creates one CAS per file, containing the file's URI
		CollectionReaderDescription reader = UriCollectionReader.getDescriptionFromFiles(files);

//		AnalysisEngineDescription textReader = UriToDocumentTextAnnotator.getDescription();
		AnalysisEngineDescription fdtbTextReader = FDTBTextReader.getDescription();
		
		for (JCas jCas : SimplePipeline.iteratePipeline(reader, fdtbTextReader)) {
			assertThat(this.jCas).isNull();
			this.jCas = jCas;
		}
	}

	@Test
	public void givenTheFDTBWhenReadingDatasetThenTheContentOfDocumentIsNotNull(){
		assertThat(jCas).isNotNull();
		assertThat(jCas.getDocumentText()).isNotNull();
	}

	@Test
	public void givenTheFDTBWhenReadingDatasetThenTheNumberOfSentenceIs18535(){
		Collection<Sentence> sentences = JCasUtil.select(jCas, Sentence.class);
		assertThat(sentences).hasSize(18535);
	}
	
	
	@Test
	public void givenTheFDTBWhenReadingDatasetThenNoPairOfSentencesHaveAnOverlap(){
		Collection<Sentence> sentences = JCasUtil.select(jCas, Sentence.class);
		
		String prevText = null;
		for (Sentence sent: sentences){
			String coveredText = sent.getCoveredText();
			if (prevText != null){
				assertThat(prevText.charAt(prevText.length() - 1)).isNotEqualTo(coveredText.charAt(0));
			}
			prevText = coveredText;
		}
	}
	
	@Test
	public void givenTheFDTBWhenReadingDatasetThenTheContentOfSentencesAreCorrect() throws IOException{
		@SuppressWarnings("unchecked")
		List<String> lines = FileUtils.readLines(new File("data/sents.txt"));
		Collection<Sentence> sentences = JCasUtil.select(jCas, Sentence.class);
		int idx = 0;
		for (Sentence sent: sentences){
			assertThat(sent.getCoveredText().trim()).isEqualTo(lines.get(idx++).trim());
		}
	}
}
