package laali.ir.fdtb;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.util.cr.UriCollectionReader;
import org.junit.Before;
import org.junit.Test;

public class GoldConnectiveAnnotatorTest {
	private JCas jCas;

	@Before
	public void setUp() throws UIMAException, IOException{
		@SuppressWarnings("unchecked")
		Collection<File> files = FileUtils.listFiles(new File("data/fdtb/fdtb1"), new String[]{".xml"}, false);

		// A collection reader that creates one CAS per file, containing the file's URI
		CollectionReaderDescription reader = UriCollectionReader.getDescriptionFromFiles(files);

//		AnalysisEngineDescription textReader = UriToDocumentTextAnnotator.getDescription();
		AnalysisEngineDescription conllSyntaxJsonReader = GoldConnectiveAnnotator.getDescription();
		
		for (JCas jCas : SimplePipeline.iteratePipeline(reader, conllSyntaxJsonReader)) {
			assertThat(this.jCas).isNull();
			this.jCas = jCas;
		}
	}

	@Test
	public void givenThePDTBWhenReadingDatasetThen7646ConnectiveAreAdded(){
		assertThat(jCas).isNotNull();

		Collection<DiscourseConnective> select = JCasUtil.select(jCas, DiscourseConnective.class);
		
		assertThat(select).isNotNull();
		assertThat(select).hasSize(7646);
	}
}
