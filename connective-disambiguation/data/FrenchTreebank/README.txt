
This document is intended for getting you started in a few minutes, but it
cannot answer all of your questions.

The French treebank is distributed in the hope it will be useful, but
WITHOUT ANY WARRANTY.

If you need help in finding information related to the treebank or anything
else, please contact anne.abeille@linguist.jussieu.fr 

Your comments can also help make the treebank much better for everyone. If
you've found something incorrect, let us know so that we can improve or correct
it.

--------------------------------
Overall organization of the CD-R
--------------------------------

Data are divided into two main directories:
  - corpus-constit directory : constituents +morphosyntactic annotations
  - corpus-fonctions directory : functions+ constituent + morphosyntactic annotations

If you have problems, please read the documentations found in the GUIDES
directory. This should help you solve most of your problems.

-----------------
The morphosyntactic annotations
-----------------

We define a complete morphosyntactic tag as follows:
  - Part of speech (POS)
  - Subcategorization
  - Inflection
  - Lemma (canonical form)
  - Parts (with similar morphosyntactic tags) for compounds.

For part of speech, we made traditional choices, except for weak pronouns that
were given a POS of their own (clitic) according to the generative tradition,
and foreign words (in quotations) which receive a special ET tag. Punctuations
are divided between strong (clause markers) and weak (all the others). Most
typographical signs (including %, numbers and abbreviations) are assigned a
traditional POS (usually common noun).

We distinguish 15 lexical categories, used for simple words as well as for
compounds:
  - A (adjective)
  - Adv (adverb)
  - CC (coordinating conjunction)
  - Cl (weak clitic pronoun)
  - CS (subordinating conjunction)
  - D (determiner)
  - ET (foreign word)
  - I (interjection)
  - NC (common noun)
  - NP (proper noun)
  - P (preposition)
  - PREF (prefix)
  - PRO (strong pronoun)
  - V (verb)
  - PONCT (punctuation mark)

-------------------------
The constituent annotation
_________________________

After the tagging phase was completed, we started the parsing phase of the
project for which we chose surface and shallow annotations, compatible with
various syntactic frameworks.

The current syntactic tagset is as follows:
  - AP (adjectival phrases)
  - AdP (adverbial phrases)
  - COORD (coordinated phrases)
  - NP (noun phrases)
  - PP (prepositional phrases)
  - VN (verbal nucleus)
  - VPinf (infinitive clauses)
  - VPpart (nonfinite clauses)
  - SENT (sentences)
  - Sint, Srel, Ssub (finite clauses)

We chose to only annotate major phrases with little internal structure. For the
sake of simplicity, we make parsimonious use of unary phrases. For rigid
sequences of categories, such as dates or addresses, it is difficult to
determine the head, and we have one global NP with no internal constituents.

We annotate certain phrases with a subcategory, which is important for
functional annotation, for example relative or subordinate for embedded clauses.

We do not have discontinuous constituents.

In order to be as therory neutral as possible, we neither use empty categories,
nor functional phrases (non DP or CP). We allow for headless phrases (elliptical
NP lacking a head noun or sentencial clauses lacking a verbal nucleus).

For verbal phrases, we only annotate the minimal verbal nucleus (clitics,
auciliaries, negation and verb), because the traditional VP (with complements)
is subject to much linguistic debate and is often discontinuous in French.

For coordination, we only mark a coordinating phrase after a coordinating
conjunction. We do not necessarily embed conjuncts inside a phrase since there
are cases where the is none, and there are cases where the category of the
phrase would be unspecified.

--------------------------
The function annotation
---------------------------
We annotate grammatical functions for the major constituents which are sisters of a verbal nucleus (VN). For dependents of Nouns or Adjectives, their function is implicitly coded by their being included inside an NP or an AP.

We use the following grammatical functions:
- SUJ (for subjects)
- OBJ (for direct objects)
- ATS (for a predicative complement linked with a subject)
- ATO (for a predicative complement linked with an object)
- A-OBJ (for indirect objects with preposition �)
- DE-OBJ (for indirect objetcs with preposition de)
- P-OBJ (for indirect objects with another preposition)
- MOD (for modifiers and adjuncts)

Notice that Adverbs rarely have a function tag, since they do not project unary constituents.
Notice also that VN bear the function of their pronominal clitics, so they can have more than one function tagged.

----------------------------

Example of morphosyntactic annotation (simplified)
--------------------------

<SENT nb="1">
  <w lemma="le" cat="D" subcat="def" mph="fs">L'</w> 
  <w compound="yes" lemma="Association de les industriels japonais"
     cat="N" subcat="P" mph="fs">
    <w catint="N">Association</w>
    <w catint="P">des</w>
    <w catint="D"/>
    <w catint="N">industriels</w>
    <w catint="A">japonais</w>
  </w>
  <w lemma="pr�voir" cat="V" subcat="" mph="P3s">pr�voit</w>
  <w lemma="un" cat="D" subcat="ind" mph="fs">une</w>
  <w lemma="l�ger" cat="A" subcat="qual" mph="fs">l�g�re</w>
  <w lemma="reprise" cat="N" subcat="C" mph="fs">reprise</w>
  <w lemma="en" cat="P">en</w>
  <w lemma="1993" cat="N" subcat="card" mph="fs">1993</w>
  <w lemma="." cat="PONCT" subcat="S">.</w>
</SENT>

---------------------------------------------------------------
Example of function + constituent annotation (simplified, without morphosyntactic annotations)
---------------------------------------------------------------

<SENT>
  <NP fct="SUJ">AGA,
    <Srel>
      <NP fct="SUJ">qui</NP>
      <VN>d�tenait</VN>
      <NP fct="OBJ">26,4 % <PP>de <NP>la CEGF</NP></PP></NP>
      <PP fct="MOD">par <NP>le biais <PP>de <NP>sa filiale Figoscandia</NP></PP></NP></PP>
    </Srel>
  </NP>
  <VN fct="OBJ">en d�tient</VN>d�sormais
  <NP fct="OBJ">77,8 %</NP>
  <COORD>et
    <VN>s'appr�te</VN>
    <VPinf fct="A-OBJ">�
      <VN>lancer</VN>
      <NP fct="OBJ">une OPA</NP>
  </COORD>
  <NP fct="MOD">ce
    <Srel>
      <NP fct="SUJ">qui</NP>
      <VN>portera</VN>
      <NP fct="OBJ">le prix <AP>total</AP><PP>de <NP>l'acquisition</NP></PP></NP>
      <PP fct="A-OBJ">� <NP>515 millions <PP>de <NP>francs</NP></PP></NP></PP>
    </Srel>
  </NP>.
</SENT>

<SENT>
  <PP fct="MOD">Au <NP>d�but</NP></PP>,
  <VN fct="SUJ">on ramassait</VN>
    <NP fct="OBJ">quinze sacs_poubelle</NP>,
  <Sint fct="MOD">
    <VN>indique</VN>
    <NP fct="SUJ">Roger, <NP>ouvrier <PP>� <NP>la r�gie</NP></PP></NP></NP>
  </Sint>.
</SENT>

----------------------------
COPYRIGHT Anne Abeill�, LLF, Paris 7 2006-04-07
